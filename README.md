# python-airflow-2

# Installation

### Create folder && virtualenv && activete
### Open git

```https://github.com/apache/airflow```

### Installing from PyPI

Install modules. We can change version like ```...-3.7.txt``` to python version.

```
pip install 'apache-airflow==2.3.2' \
 --constraint "https://raw.githubusercontent.com/apache/airflow/constraints-2.3.2/constraints-3.7.txt"

 pip install 'apache-airflow[postgres,google]==2.3.2' \
 --constraint "https://raw.githubusercontent.com/apache/airflow/constraints-2.3.2/constraints-3.7.txt"
```

Use absolute path.

```
export AIRFLOW_HOME=/home/user/projects/python-airflow-2/airflow
```

### INIT db

```
airflow db init
```

### Create user

Open man.

```
airflow users create --help
```

```
airflow users create --username admin --firstname admin --lastname admin --role Admin --email admin@example.com
```

And set the password

### Run server

```
airflow webserver -p 8080
```

Open http://localhost:8080.

### Start Scheduler

```
airflow scheduler
```

# Inside docker

https://airflow.apache.org/docs/apache-airflow/stable/start/docker.html


### Download docker-compose.yml

```
curl -LfO 'https://airflow.apache.org/docs/apache-airflow/2.3.3/docker-compose.yaml'
```

### Set user

```
echo -e "AIRFLOW_UID=$(id -u)\nAIRFLOW_GID=0" > .env
```

```
mkdir -p ./dags ./logs ./plugins
echo -e "AIRFLOW_UID=$(id -u)" > .env
```


### Initialize the database

docker-compose up airflow-init


### Login/Password on localhost:8080

```
airflow
airflow
```

then

```
docker-compose up -d
docker-compose down -v
```

### Install custom packages

- create requirements.txt file
- create Dockerfile
- run ```docker build . --tag extending_airflow:latest```
- open docker-compose file
- change line 47 ```image: ${AIRFLOW_IMAGE_NAME:-apache/airflow:2.3.3}``` -> ```image: ${AIRFLOW_IMAGE_NAME:-extending_airflow:latest}```
- restart containers

### MiniIO

https://airflow.apache.org/docs/apache-airflow-providers-amazon/stable/index.html
https://airflow.apache.org/docs/apache-airflow-providers-amazon/4.0.0/_api/airflow/providers/amazon/index.html

- run Docker image locally
```
docker run \
  -p 9000:9000 \
  -p 9001:9001 \
  -e "MINIO_ROOT_USER=AKIAIOSFODNN7EXAMPLE" \
  -e "MINIO_ROOT_PASSWORD=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY" \
  quay.io/minio/minio server /data --console-address ":9001"
```
- create a bucket
- create ```data``` folder
- upload files into bucket
- create connection 'minio' and use Extra as ```{"aws_access_key_id": "AKIAIOSFODNN7EXAMPLE","aws_secret_access_key": "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY","host": "http://host.docker.internal:9000"}```