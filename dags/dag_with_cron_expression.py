from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.bash import BashOperator

default_arguments = {
    'owner': 'bda82',
    'retries': 5,
    'retry_delay': timedelta(minutes=5)
}

with DAG(
    default_args=default_arguments,
    dag_id='dag_with_cron_expression_v02',
    start_date=datetime(2022, 8, 1),
    schedule_interval='0 3 * * Thu'  # At 03:00 on Thursday. In DAGS Nex run will be "2022-08-04, 03:00:00"
    # 0 3 * * Thu,Fri -> At 03:00 on Thursday and Friday.
    # 0 3 * * Thu-Fri -> At 03:00 on every day-of-week from Thursday through Friday.
) as dag:
    task1 = BashOperator(
        task_id='task1',
        bash_command='echo Dag with cron expression'
    )

    task1