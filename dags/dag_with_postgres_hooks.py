import csv
import logging
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.providers.amazon.aws.hooks.s3 import S3Hook

logger = logging.getLogger(__name__)


default_args = {"owner": "bda82", "retries": 5, "retry_delay": timedelta(minutes=5)}


def postgres_to_s3():
    # step 1: query data from postgres db and save into text file
    # step 2: upload text file into s2 bucket
    hook = PostgresHook(postgres_conn_id="postgres13")
    conn = hook.get_conn()
    cursor = conn.cursor()
    cursor.execute("select * from orders")
    with open("dags/get_orders.csv", "w") as f:
        csv_writer = csv.writer(f)
        csv_writer.writerow([i[0] for i in cursor.description])
        csv_writer.writerows(cursor)
    cursor.close()
    conn.close()
    logger.info("Complete write data into file")
    s3_hook = S3Hook(aws_conn_id="minio")
    s3_hook.load_file(
        filename="dags/get_orders.txt", key="orders/orders.csv", bucket_name="airflow"
    )


with DAG(
    dag_id="dag_with_postgres_hooks_v01",
    default_args=default_args,
    start_date=datetime(2022, 8, 1),
    schedule_interval="0 0 * * *",
) as dag:
    task1 = PythonOperator(task_id="postgres_to_file", python_callable=postgres_to_s3)
    task1

# https://airflow.apache.org/docs/apache-airflow-providers-postgres/stable/index.html
