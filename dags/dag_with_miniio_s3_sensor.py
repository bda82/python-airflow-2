from datetime import datetime, timedelta

from airflow import DAG
from airflow.providers.amazon.aws.sensors.s3_key import S3KeySensor


default_args = {"owner": "bda82", "retries": 5, "retry_delay": timedelta(minutes=5)}

with DAG(
    dag_id="dag_with_miniio_s3_sensor_v02",
    start_date=datetime(2022, 8, 1),
    schedule_interval="@daily",
    default_args=default_args,
) as dag:
    task = S3KeySensor(
        task_id="sensor_minio_s3",
        bucket_name="airflow",
        bucket_key="data.csv",
        aws_conn_id="minio",
        mode="poke",
        poke_interval=5,
        timeout=30,
    )
