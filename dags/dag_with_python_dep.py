from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.python import PythonOperator


default_arguments = {
    "owner": "bda82",
    "retries": 5,
    "retry_delay": timedelta(minutes=5),
}


def get_sklearn():
    import sklearn

    print(f"scikit-learn with version: {sklearn.__version__}")


with DAG(
    default_args=default_arguments,
    dag_id="dag_with_python_dep_v01",
    start_date=datetime(2022, 8, 1),
    schedule_interval="@daily",
) as dag:

    task1 = PythonOperator(task_id="get_sklearn", python_callable=get_sklearn)

    task1
