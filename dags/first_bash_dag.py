from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash import BashOperator

default_args = {
    'owner': 'bda82',
    'retries': 5,
    'retry_delay': timedelta(minutes=1)
}

with DAG(
    dag_id='first_bash_dag_v3',
    description='First DAG',
    start_date=datetime(2022, 7, 12),
    schedule_interval='@hourly',
    default_args=default_args,
) as dag:
    task1 = BashOperator(
        task_id='first_task',
        bash_command='echo hello world from first task',
    )

    task2 = BashOperator(
        task_id='second_task',
        bash_command='echo hello world Task 2 after first task',
    )

    task3 = BashOperator(
        task_id='third_task',
        bash_command='echo hello world Task 3 after first task',
    )

    task1.set_downstream(task2)
    task1.set_downstream(task3)
    # or
    # task1 >> task2
    # task1 >> task3
    # or
    # task1 >> [task2, task3]