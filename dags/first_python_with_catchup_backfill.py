from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.bash import BashOperator


default_arguments = {
    'owner': 'bda82',
    'retries': 5,
    'retry_delay': timedelta(minutes=5)
}

with DAG(
    dag_id='dag_with_catchup_backfill_v01',
    default_args=default_arguments,
    start_date=datetime(2022, 7, 31),
    schedule_interval='@daily',
    catchup=False
) as dag:
    task1 = BashOperator(
        task_id='task1',
        bash_command='echo This is simple bash command'
    )

    # docker exec -it python-airflow-2_airflow-scheduler_1 bash
    # airflow dags backfill -s 2022-07-31 -e 2022-8-3 dag_with_catchup_backfill_v01