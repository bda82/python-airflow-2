from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python import PythonOperator
from anyio import TASK_STATUS_IGNORED, getnameinfo

default_args = {
    'owner': 'bda82',
    'retries': 5,
    'retry_delay': timedelta(minutes=1),
}

def get_name(ti):
    ti.xcom_push(key='first_name', value='Dmitry')
    ti.xcom_push(key='last_name', value='Bes')


def greet(ti):
    first_name = ti.xcom_pull(task_ids='get_name', key='first_name')
    last_name = ti.xcom_pull(task_ids='get_name', key='last_name')
    age = ti.xcom_pull(task_ids='get_age', key='age')
    print('Hello from Python DAG. My name is %s %s and i am age %s years old' % (first_name, last_name, age))


def get_age(ti):
    ti.xcom_push(key='age', value=40)


with DAG(
    default_args=default_args,
    dag_id='first_python_dag_v4',
    description='First python DAG with parameters passing through xcoms',
    start_date=datetime(2022, 7, 12),
    schedule_interval='@daily'
) as dag:
    task1 = PythonOperator(
        task_id='get_name',
        python_callable=get_name,
    )

    task2 = PythonOperator(
        task_id='get_age',
        python_callable=get_age
    )
    
    task3 = PythonOperator(
        task_id='greet',
        python_callable=greet,
    )


    [task1, task2] >> task3