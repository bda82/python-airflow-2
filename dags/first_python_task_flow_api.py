from datetime import datetime, timedelta

from airflow.decorators import dag, task


default_args = {
    'owner': 'bda82',
    'retries': 5,
    'retry_delay': timedelta(minutes=5)
}


@dag(
    dag_id='dag_with_taskflow_api_v01',
    default_args=default_args,
    start_date=datetime(2022, 7, 25),
    schedule_interval='@daily'
)
def hello_etl():

    @task()
    def get_name():
        return 'Dima'

    @task()
    def get_age():
        return 40

    @task()
    def greet(name, age):
        print(f"Hello {name} {age}")

    name = get_name()
    age = get_age()
    greet(name=name, age=age)

greet_dag = hello_etl()