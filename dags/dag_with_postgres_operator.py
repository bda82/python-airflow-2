from datetime import datetime, timedelta

from airflow import DAG
from airflow.providers.postgres.operators.postgres import PostgresOperator

default_arguments = {
    "owner": "bda82",
    "retries": 5,
    "retry_delay": timedelta(minutes=5),
}

with DAG(
    dag_id="dag_with_postgres_operator_v01",
    default_args=default_arguments,
    start_date=datetime(2022, 8, 1),
    schedule_interval="0 0 * * *",
) as dag:
    task1 = PostgresOperator(
        task_id="create_postgres_table",
        postgres_conn_id="postgres13",
        sql="""
        create table if not exists dag_runs (
            dt date,
            dag_id character varying,
            primary key (dt, dag_id)
        )        
        """,
    )

    # Airflow Template Reference: https://airflow.apache.org/docs/apache-airflow/stable/templates-ref.html

    task2 = PostgresOperator(
        task_id="delete_from_table",
        postgres_conn_id="postgres13",
        sql="""
        delete from dag_runs where dt = '{{ds}}' and dag_id = '{{dag.dag_id}}'
        """,
    )

    task3 = PostgresOperator(
        task_id="insert_into_table",
        postgres_conn_id="postgres13",
        sql="""
        insert into dag_runs (dt, dag_id) values ('{{ds}}', '{{dag.dag_id}}')
        """,
    )

    task1 >> task2 >> task3
